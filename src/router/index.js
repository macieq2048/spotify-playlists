import Vue from 'vue'
import VueRouter from 'vue-router'
import Playlists from '@/views/Playlists'
import Welcome from "@/views/Welcome";
import Authorize from "@/views/Authorize";
import SavedTracks from "@/views/SavedTracks";
import CreatePlaylist from "@/views/CreatePlaylist";
import MainContainer from "@/components/containers/MainContainer";


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Welcome',
        component: Welcome,
        props: true,
    },
    {
        path: '/authorize',
        name: 'Authorize',
        component: Authorize,
    },
    {
        path: '',
        component: MainContainer,
        children: [
            {
                path: '/playlists',
                name: 'Playlists',
                component: Playlists,
            },
            {
                path: '/saved-tracks',
                name: 'SavedTracks',
                component: SavedTracks,
            },
            {
                path: "/create-playlist",
                name: "CreatePlaylists",
                component: CreatePlaylist,
            },
        ]
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
export default router