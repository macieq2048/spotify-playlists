import SpotifyWebApi from "spotify-web-api-js";
import {SPOTIFY_TOKEN_EXPIRATION_DURATION} from "@/constatns/spotify";
import vue from "@/main"

export const isTokenExpired = () => {
    const savedToken = JSON.parse(localStorage.getItem("authorization_token"))
    if(!savedToken) return true;
    const now = parseInt(new Date().getTime().toString());
    return now - savedToken.timestamp > SPOTIFY_TOKEN_EXPIRATION_DURATION;
}

export const setToken = (token) => {
    const object = {token: token, timestamp: new Date().getTime()}
    localStorage.setItem("authorization_token", JSON.stringify(object));
}

export const getToken = () => {
    if (isTokenExpired()){
        vue.$router.push({name: "Welcome", params: {beginWithStep: 1}});
        return;
    }
    const savedToken = JSON.parse(localStorage.getItem("authorization_token"))
    return savedToken.token;
}

export const getUserSavedTracks = async (offset= 0, limit=50) => {
    const spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(getToken());
    let data_c = {}
    await spotifyApi
        .getMySavedTracks({offset: offset, limit: limit})
        .then(
            (data) => {
                data_c = data;
            },
            function (err) {
                data_c = err;
            }
        );
    return data_c;
}

//todo
export const createPlaylist = async (playlistName) => {
    const spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(getToken());
    const user = await spotifyApi.getMe();
    return await spotifyApi.createPlaylist(user.id, {name: playlistName});
}
//todo
// eslint-disable-next-line no-unused-vars
export const getSongsInPlaylist = async (playlistId) => {
    const spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(getToken());
    return await spotifyApi.getPlaylistTracks(playlistId);
}

export const testtest = async()=>{
    console.log(vue)
};


//todo
// eslint-disable-next-line no-unused-vars
export const addSongsToPlaylist = async (tracks, playlistId, allowDuplicates = false) => {
    const spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(getToken());
    const tracksUris = this.selectedTracks.map((track) => {
        return track.track.uri;
    })
    return await spotifyApi.addTracksToPlaylist(playlistId, tracksUris);
}