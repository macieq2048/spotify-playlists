import {encode} from 'querystring';

export const request = async ({
                                  url,
                                  query,
                                  headers = {},
                                  method = 'GET',
                                  body,
                                  formData,
                              }) => {
    if (!headers['Content-Type'] && !formData) {
        headers['Content-Type'] = 'application/json';
    }
    if (body && typeof body === 'object' && !formData) {
        body = JSON.stringify(body);
    }
    if (formData) {
        body = formData;
    }
    const res = await fetch(url + (query ? `?${encode(query)}` : ''), {
        method,
        headers,
        body,
    });


    let data;
    try {
        data = await res.json();
    } catch {
        data = {message: res.statusText};
    }

    return {
        res,
        data,
    };
};
