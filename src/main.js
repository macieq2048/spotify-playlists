import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
const Spotify = require('spotify-web-api-js');
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


Vue.use(vuetify);
Vue.use(Spotify);
Vue.use(VueSweetalert2);

const vue = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  vuetify,

  components: {
    App
  }
});

export default vue;