const SPOTIFY_API_BASE = "https://accounts.spotify.com/authorize"

export const SPOTIFY_API_ENDPOINTS ={
    base: SPOTIFY_API_BASE,
}